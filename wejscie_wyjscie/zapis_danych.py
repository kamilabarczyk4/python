from os import path

if(not path.exists("password.txt")):
    password = open("password.txt", 'w+')
    password.write(input("Podaj nowy kod: "))
    password.close()

if(path.exists("password.txt")):
    password = open("password.txt", 'r')
    correct_password = password.readline()
    password.close()

    entered_password = input("Podaj poprawny kod: ")

    if(entered_password == correct_password):
        print("Kod poprawny")
    else:
        print("Kod niepoprawny")
