from PIL import Image
import os

dir = os.path.dirname(os.path.abspath(__file__))

pictures = [dir+"/zdjecia/koty.jpg", dir+"/zdjecia/pies.jpg",
            dir+"/zdjecia/foka.jpg", dir + "/zdjecia/tygrys.jpg"]
for item in pictures:
    image = Image.open(item)
    image.save(item.replace(".jpg", ".png"))
