from numpy import dot

a = [1, 2, 12, 4]
b = [2, 4, 2, 8]

# wbudowana funkcja
print("wynik z funkcji wbudowanej: " + str(dot(a, b)))

# funckja do obliczania iloczynu skalarnego dla dowolnych wektorów


def iloczyn_skalarny(x, y):
    return sum(x[i]*y[i] for i in range(len(x)))


print("wynik z zaimplementowanej metody: " + str(iloczyn_skalarny(a, b)))
