import os
from os import path

dir = os.path.dirname(__file__)

if(path.exists(dir+"/text_in.txt")):
    file = open(dir+"/text_in.txt", 'r')
    text = file.read()
    file.close()

    dictionary = {"i": "oraz",
                  "oraz": "i",
                  "nigdy": "prawie nigdy",
                  "dlaczego": "czemu"}

    for word in dictionary:
        text = text.replace(word, dictionary[word])

    file1 = open(dir+"/text_out2.txt", "w")
    file1.write(text)
    file1.close()
