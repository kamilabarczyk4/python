from complex_class import complex


def main():
    print("***Kalkulator liczb zespolonych***")
    real1 = float(input("Podaj część rzeczywistą pierwszej liczby: "))
    imaginary1 = float(input("Podaj część urojona pierwszej liczby: "))

    real2 = float(input("Podaj część rzeczywistą drugiej liczby: "))
    imaginary2 = float(input("Podaj część urojona drugiej liczby: "))

    complex1 = complex(real1, imaginary1)
    complex2 = complex(real2, imaginary2)

    print("\nPierwsza liczba: ")
    complex1.print()
    print("\nDruga liczba: ")
    complex2.print()

    ans = float(input(
        "\nWybierz operację:\n1. Dodawanie\n2. Odejmowanie\n3. Mnożenie\n4. Moduł\n5. Faza\n"))
    if ans == 1:
        (complex1+complex2).print()
    if ans == 2:
        (complex1-complex2).print()
    if ans == 3:
        (complex1*complex2).print()
    if ans == 4:
        print("Moduł pierwszej liczby: ", complex1.mag())
        print("Moduł drugiej liczby: ", complex2.mag())
    if ans == 5:
        print("Faza pierwszej liczby: ", complex1.ang())
        print("Faza drugiej liczby: ", complex2.ang())


if __name__ == '__main__':
    main()
